#!/usr/bin/env python
# coding: utf-8

# In[19]:


import numpy as np
import cv2
import math
import os
import subprocess


# In[22]:


def main(path):
    n = 0
    for entry in os.scandir(path):
        if entry.is_file():
            n = n + 1
    # print(n)

    s = (n, n)
    nameList = ["" for x in range(n)]
    distances = np.zeros(s)
    # print(names)

    i = 0
    for entry in os.scandir(path):
        if entry.is_file():
            nameList[i] = entry.name
            i = i + 1
    folder = path
    heightList = []
    widthList = []
    for i in range(n):
        img = cv2.imread(folder+''+nameList[i],0)
        height, width = img.shape
        heightList.append(height)
        widthList.append(width)
    return nameList, heightList, widthList, n

def deleteImages(path, n, nameList, heightList, widthList, Th, Tw):
    deletedList = []
    for i in range(n):
        filename = path+''+nameList[i]
#         print(os.path.exists(filename))
        if(os.path.exists(filename) == True):
            if heightList[i] <= Th or widthList[i] <= Tw:
                subprocess.call('rm -r '+filename,shell=True)
                deletedList.append('Deleted')
            else:
                deletedList.append('Not Deleted')
        else:
            deletedList.append('Not Deleted')
    return deletedList
                


# In[25]:


Th = 200
Tw = 200
path = 'images/'
nameList, heightList, widthList, n = main(path)
# print(nameList, heightList, widthList, n)
deletedList = deleteImages(path, n, nameList, heightList, widthList, Th, Tw)
for i in range(n):
    print(nameList[i], "(Size height x width:", heightList[i],'x',widthList[i],"(Status:", deletedList[i], ")")


# In[ ]:




